package co.edu.uis.mibarrio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MibarrioApplication {

	public static void main(String[] args) {
		SpringApplication.run(MibarrioApplication.class, args);
	}

}
