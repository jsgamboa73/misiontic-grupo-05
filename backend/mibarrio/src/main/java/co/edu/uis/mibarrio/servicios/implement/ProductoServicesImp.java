/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.uis.mibarrio.servicios.implement;

import co.edu.uis.mibarrio.dao.ProductosDao;
import co.edu.uis.mibarrio.modelos.ProductosEntity;
import co.edu.uis.mibarrio.servicios.ProductoServices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author GRUPO 05
 */
@Service
public class ProductoServicesImp implements ProductoServices {
    @Autowired
    private ProductosDao repositorioProducto;
    
    @Override
    @Transactional(readOnly=false)                                              
    public ProductosEntity save(ProductosEntity producto){
        return repositorioProducto.save(producto);
    }
    
    @Override
    @Transactional(readOnly=false)                                              
    public void delete(Integer id){
        repositorioProducto.deleteById(id);
    }
    
    
    @Override
    @Transactional(readOnly=true)
    public ProductosEntity findById(Integer id){
        return repositorioProducto.findById(id).orElse(null);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<ProductosEntity> findAll(){
        return (List<ProductosEntity>) repositorioProducto.findAll();
    }
}
