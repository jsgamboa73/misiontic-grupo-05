package co.edu.uis.mibarrio.controladores;

import co.edu.uis.mibarrio.modelos.ProductosEntity;
import co.edu.uis.mibarrio.servicios.ProductoServices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin("*")
@RequestMapping("/api/productos/")
public class ProductosController {
    @Autowired
    private ProductoServices servicioProductos;
    
    @GetMapping("/list")
    public List<ProductosEntity> consultarProductos(){
        return servicioProductos.findAll();
    }
    
    @GetMapping("list/{id}")
    public ProductosEntity buscarPorId(@PathVariable Integer id){
        return servicioProductos.findById(id);
    }
    
    @PostMapping(value="/add", consumes = {"application/json"})
    public ResponseEntity<ProductosEntity> agregar (@RequestBody ProductosEntity producto){
        
        ProductosEntity registro = servicioProductos.save(producto);
        return new ResponseEntity<>(registro, HttpStatus.OK);
    }
    @DeleteMapping(value="/delete/{id}")
    public ResponseEntity<ProductosEntity> eliminar (@PathVariable Integer id){
            ProductosEntity registroPorId = servicioProductos.findById(id);
            
            if(registroPorId != null){
                servicioProductos.delete(id);
         
            } else {
                return new ResponseEntity<>(registroPorId, HttpStatus.INTERNAL_SERVER_ERROR);
                
            }
            return new ResponseEntity<>(registroPorId, HttpStatus.OK);
    }
    
    @PutMapping(value="/edit")
    public ResponseEntity<ProductosEntity> editar (@RequestBody ProductosEntity producto){
        ProductosEntity productoPorId = servicioProductos.findById(producto.getIdproductos());
        if(productoPorId != null){
            productoPorId.setNombreProducto(producto.getNombreProducto());
            productoPorId.setValorCosto(producto.getValorCosto());
            productoPorId.setValorVenta(producto.getValorVenta());
            servicioProductos.save(productoPorId);
        } else {
            return new ResponseEntity<>(productoPorId, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(productoPorId, HttpStatus.OK);
    }
    
    
    
}
