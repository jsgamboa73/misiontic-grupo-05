/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.uis.mibarrio.servicios;

import co.edu.uis.mibarrio.modelos.ProductosEntity;
import java.util.List;

/**
 *
 * @author GRUPO 05
 */
public interface ProductoServices {
    public ProductosEntity save (ProductosEntity producto);
    public void delete(Integer id);
    public ProductosEntity findById(Integer id);
    public List<ProductosEntity> findAll();
    
    
    
}
