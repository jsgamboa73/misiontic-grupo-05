/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.uis.mibarrio.dao;

import co.edu.uis.mibarrio.modelos.ProductosEntity;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author GRUPO 05
 */
public interface ProductosDao extends CrudRepository<ProductosEntity, Integer > {
    
    
}
