const LISTAR = "http://localhost:8080/api/productos/list";


$(document).ready(function(){

    
    // AGREGAR PRODUCTOS A LA TABLA
    $.ajax({
        url: LISTAR,
        dataType: "json",
        method: "GET"
    }).done(function(response){
        let resProductos = response.slice().reverse();


        $.each(resProductos, function(i, item){
            
            
            let btnEliminar = "<button type=\"button\" class=\"btn btn-danger btnEliminar\" name=\"" + item.idproductos +"\"><i class=\"bi bi-trash\"></i>&nbsp;Eliminar</button>";
            let btnEditar ="<button type=\"button\" class=\"btn btn-success btnEditar\"  name=\"" + item.idproductos +"\" data-bs-toggle=\"modal\" data-bs-target=\"#editarProductoModal\"><i class=\"bi bi-pencil-square\"></i>&nbsp;Editar</button>";

            let filaHTML = "<tr data-filaid=" + item.idproductos + ">";
            filaHTML += "<th scope=\"row\">" + item.idproductos + "</th>";
            filaHTML += "<td>"+ item.nombreProducto +"</td>";
            filaHTML += "<td>$"+ item.valorCosto +"</td>";
            filaHTML += "<td>$"+ item.valorVenta +"</td>";
            filaHTML += "<td>$"+ (item.valorVenta - item.valorCosto) +"</td>";
            filaHTML += "<td>"+ btnEditar + "&nbsp;" + btnEliminar +"</td>";
            filaHTML += "</tr>";

            $("#tablaProductos tbody").append(filaHTML);
            
        });

        $(".btnEliminar").click(function (e) { 
            let idProductoEliminar = $(this).attr("name");
            $.ajax({
                url: "http://localhost:8080/api/productos/delete/" + idProductoEliminar,
                contentType: "aplication/json; charset=utf-8",
                dataType: "json",
                type: "json",
                method: "DELETE"

            }).done(function(res){
                $("[data-filaid=" + idProductoEliminar + "]").remove();
            });
            
        });

        $(".btnEditar").click(function(e){
            $.ajax({
                url: "http://localhost:8080/api/productos/list/" + $(this).attr("name"),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "json",
                method: "GET",
            }).done(function(res){
                console.log(res)
                $("#inputEditarNombreProducto").val(res.nombreProducto);
                $("#inputEditarValorCosto").val(res.valorCosto);
                $("#inputEditarValorVenta").val(res.valorVenta);

                $("#botonModalEditar").click(function(e){
                    console.log("Boton clickeado modalagregar");
                    $.ajax({
                        url: "http://localhost:8080/api/productos/edit/" + res.idproductos,
                        data: JSON.stringify({
                            "nombreProducto": $("#inputEditarNombreProducto").val(),
                            "valorCosto": $("#inputEditarValorCosto").val(),
                            "valorVenta": $("#inputEditarValorVenta").val()                            
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        type: "json",
                        method: "PUT",
                        success: function(res){
                            window.location.reload();
                        }
                    });
                })
            });
            
        });

        


    });



    $("#botonModalAgregar").unbind().click(function (e) { 
        $.ajax({
            url: "http://localhost:8080/api/productos/add",
            data: JSON.stringify(
                {
                    "nombreProducto": $("#inputNombreProducto").val(),
                    "valorCosto": $("#inputValorCosto").val(),
                    "valorVenta": $("#inputValorVenta").val()                    
                }
            ),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "json",
            method: "POST",
            success: function(res){
                console.log(res)
                $("#formularioAgregarProducto").trigger("reset");
                bootstrap.Modal.getInstance("#exampleModal").hide();
                window.location.reload();
            }
        });
        
    });

});