-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: tienda
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `detalles`
--

DROP TABLE IF EXISTS `detalles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detalles` (
  `iddetalle` int NOT NULL AUTO_INCREMENT,
  `productos_idproductos` int NOT NULL,
  `transacciones_idtransaccion` int NOT NULL,
  `cantidad` int NOT NULL,
  `total` double NOT NULL,
  `ganancia` double NOT NULL,
  PRIMARY KEY (`iddetalle`,`productos_idproductos`,`transacciones_idtransaccion`),
  UNIQUE KEY `iddetalle_UNIQUE` (`iddetalle`),
  KEY `fk_productos_has_transacciones_transacciones1_idx` (`transacciones_idtransaccion`),
  KEY `fk_productos_has_transacciones_productos_idx` (`productos_idproductos`),
  CONSTRAINT `fk_productos_has_transacciones_productos` FOREIGN KEY (`productos_idproductos`) REFERENCES `productos` (`idproductos`),
  CONSTRAINT `fk_productos_has_transacciones_transacciones1` FOREIGN KEY (`transacciones_idtransaccion`) REFERENCES `transacciones` (`idtransaccion`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalles`
--

LOCK TABLES `detalles` WRITE;
/*!40000 ALTER TABLE `detalles` DISABLE KEYS */;
INSERT INTO `detalles` VALUES (2,3,1,1,1100,350),(3,7,1,2,7000,1000),(4,17,1,1,3500,750),(5,28,1,2,1000,700),(6,14,2,2,7800,1300),(7,22,2,1,2500,700),(8,26,3,2,7200,2000),(9,15,3,1,3700,850),(10,16,3,1,1200,400),(11,30,4,2,3000,1000),(12,19,4,5,2500,750),(13,24,4,1,3700,850);
/*!40000 ALTER TABLE `detalles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productos` (
  `idproductos` int NOT NULL AUTO_INCREMENT,
  `nombreProducto` varchar(45) NOT NULL,
  `valorCosto` double NOT NULL,
  `valorVenta` double NOT NULL,
  PRIMARY KEY (`idproductos`),
  UNIQUE KEY `idproductos_UNIQUE` (`idproductos`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,'Leche Litro',2500,3000),(2,'Leche Mediana',1500,2350),(3,'Leche Pequeña',750,1100),(4,'Panela',2500,3000),(5,'Azucar Blanca',4000,4800),(6,'Azucar Morena',4800,5800),(7,'Arroz',3000,3500),(8,'Lentejas',2750,3500),(9,'Frijoles',3000,3800),(10,'Garbanzos',2750,3600),(11,'Pure de Tomate',6500,7850),(12,'Avena',3200,4350),(13,'Aji',1800,2500),(14,'Sal',3250,3900),(15,'Gelatina',2850,3700),(16,'Miel',800,1200),(17,'Mayonesa',2750,3500),(18,'Vinagre',2300,3500),(19,'Huevo',350,500),(20,'Fideos',2800,3500),(21,'Mantequilla',4000,4350),(22,'Limones',1800,2500),(23,'Leche Evaporada',3250,3900),(24,'Leche Condensada',2850,3700),(25,'Tortillas de Maiz',3500,4200),(26,'Tortillas Bimbo',2600,3600),(27,'Tortillas de Harina',2800,3500),(28,'Ajo',350,500),(29,'Zanahoria',1750,2800),(30,'Perejil',1000,1500);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transacciones`
--

DROP TABLE IF EXISTS `transacciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transacciones` (
  `idtransaccion` int NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) NOT NULL,
  `total` double NOT NULL,
  `fecha` datetime NOT NULL,
  `concepto` varchar(45) DEFAULT NULL,
  `totalGanancia` double NOT NULL,
  PRIMARY KEY (`idtransaccion`),
  UNIQUE KEY `idtransaccion_UNIQUE` (`idtransaccion`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transacciones`
--

LOCK TABLES `transacciones` WRITE;
/*!40000 ALTER TABLE `transacciones` DISABLE KEYS */;
INSERT INTO `transacciones` VALUES (1,'venta',12600,'2022-09-02 14:34:44','Venta',2800),(2,'venta',10300,'2022-09-02 14:46:45','Venta',2000),(3,'venta',12100,'2022-09-02 15:01:45','Venta',3250),(4,'venta',9200,'2022-09-02 15:22:45','Venta',2600),(5,'gasto',30000,'2022-09-02 15:24:45','Compra de insumos',0),(6,'gasto',5000,'2022-09-02 15:25:45','Gastos papeleria',0),(7,'gasto',2000,'2022-09-02 15:26:45','Gasto caja menor',0);
/*!40000 ALTER TABLE `transacciones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-05 21:30:09
